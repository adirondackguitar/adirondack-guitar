Adirondack Guitar is your go-to provider of left and right-handed guitar, bass, and everything else you need to get started, jam with friends, or play gigs with your band. Serving our customers on a personal basis we get to know you, your preferences and playing style so we can provide you the best advice possible. Whether youre just beginning or have been playing in a band for years, Adirondack Guitar has the quality products you need. 20 years of leading the market doesnt happen by chance.

Address: 3 Lafayette St, Hudson Falls, NY 12839

Phone: 518-746-9500